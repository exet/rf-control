#!/bin/bash  

# Stop script on error
set -e

# Load environment
source tools/env.sh


echo ""
echo "Building Sleepo Secure Bootloader..."

cd src/dfu/secure_bootloader/pca10040_s132_ble/ses
rm -f Output/Release/Exe/secure_bootloader_ble_s132_pca10040.hex

"$SES""\bin\emBuild.exe" -rebuild -config "Release" secure_bootloader_ble_s132_pca10040.emProject


echo ""
echo "Building Sleepo Firmware..."

cd ../../../../../

"$SES""\bin\emBuild.exe" -rebuild -config "Release" sleepo.emProject

echo ""
echo "Create packages..."

cd tools
mkdir -p ../Output/hex

make all

cd ..

# Read version
version=$(<VERSION)

# Delete '"' if exist. For example 0.40.0 instead of "0.40.0"
# https://bit.ly/3oGwWgP
version="${version%\"}"
version="${version#\"}"


cd Output

mv sleepo_dfu_package.zip sleepo_dfu_package_"$version".zip
mv sleepo.hex sleepo_"$version".hex

"$ZIP""\7z" a sleepo_"$version".hex.zip sleepo_"$version".hex
rm sleepo_"$version".hex


if [ "$1" == "flash" ]; then
	cd ..
    ./flash.sh Output/sleepo_"$version".hex.zip
fi
