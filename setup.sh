#!/bin/bash  

# Stop script on error
set -e


mkdir -p tools
cd tools


./setup-toolchains.sh
./setup-libs.sh


echo ""
echo "Done!"
