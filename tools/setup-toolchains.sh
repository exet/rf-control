#!/bin/bash  

# Stop script on error
set -e


echo ""
echo "Downloading nrfutil..."

# https://github.com/NordicSemiconductor/pc-nrfutil/releases
curl -L https://github.com/NordicSemiconductor/pc-nrfutil/releases/download/v6.1/nrfutil.exe -o nrfutil.exe

#./nrfutil version


echo ""
echo "Downloading nRF Command Line Tools..."

# https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Command-Line-Tools/Download
curl -L https://www.nordicsemi.com/-/media/Software-and-other-downloads/Desktop-software/nRF-command-line-tools/sw/Versions-10-x-x/nRF-Command-Line-Tools_10_5_0_Installer_64.exe -o nRF-Command-Line-Tools_10_5_0_Installer_64.exe


echo ""
echo "Install nRF Command Line Tools..."

# Run Installer and remove it
./nRF-Command-Line-Tools_10_5_0_Installer_64.exe
rm nRF-Command-Line-Tools_10_5_0_Installer_64.exe

#mergehex --version


echo ""
echo "Downloading Segger Embedded Studio..."

# https://www.segger.com/downloads/embedded-studio/
curl -L https://www.segger.com/downloads/embedded-studio/Setup_EmbeddedStudio_ARM_v532_win_x64.exe -o Setup_EmbeddedStudio_ARM_v532_win_x64.exe


echo ""
echo "Install Segger Embedded Studio..."

# Run Installer and remove it
./Setup_EmbeddedStudio_ARM_v532_win_x64.exe
rm Setup_EmbeddedStudio_ARM_v532_win_x64.exe

# Create variable for SES IDE path
echo "SES=\"C:\Program Files\SEGGER\SEGGER Embedded Studio for ARM 5.32\"" > env.sh


echo ""
echo "Downloading 7-Zip..."

# https://www.7-zip.org/download.html
curl -L https://www.7-zip.org/a/7z1900-x64.exe -o 7z1900-x64.exe


echo ""
echo "Install 7-Zip..."

# Run Installer and remove it
# NOTE!!! Must add 7-Zip path to PATH variable environment
./7z1900-x64.exe
rm 7z1900-x64.exe

# Add variable for 7-Zip path
echo "ZIP=\"C:\Program Files\7-Zip\"" >> env.sh


echo ""
echo "Downloading gcc-arm-none-eabi-4_9-2015q3..."

curl -L https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update/+download/gcc-arm-none-eabi-4_9-2015q3-20150921-win32.exe -o gcc-arm-none-eabi-4_9-2015q3-20150921-win32.exe

./gcc-arm-none-eabi-4_9-2015q3-20150921-win32.exe
rm gcc-arm-none-eabi-4_9-2015q3-20150921-win32.exe

# Add variable for ARM-GCC path
echo "ARMGCC=\"C:\Program Files (x86)\GNU Tools ARM Embedded\4.9 2015q3\bin\"" >> env.sh
echo "ARMGCC_VERSION=\"4.9.3\"" >> env.sh


echo ""
echo "Toolchains Installed!"
