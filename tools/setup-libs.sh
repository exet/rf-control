# ===============================================================
# Setup Packages
# ===============================================================

set -e

mkdir -p ../libs
cd ../libs


echo ""
echo "Install nRF5 SDK..."

curl -L https://www.nordicsemi.com/-/media/Software-and-other-downloads/SDKs/nRF5/Binaries/nRF5SDK160098a08e2.zip -o nRF5SDK160098a08e2.zip

unzip nRF5SDK160098a08e2.zip -d nRF5_SDK_16.0.0_98a08e2
rm nRF5SDK160098a08e2.zip


echo ""
echo "Configure toolchain path..."

source ../tools/env.sh

cd nRF5_SDK_16.0.0_98a08e2/components/toolchain/gcc

# Delete '"' if exist. For example 0.40.0 instead of "0.40.0"
# https://bit.ly/3oGwWgP

ARMGCC="${ARMGCC%\"}"
ARMGCC="${ARMGCC#\"}"

ARMGCC_VERSION="${ARMGCC_VERSION%\"}"
ARMGCC_VERSION="${ARMGCC_VERSION#\"}"

echo "GNU_INSTALL_ROOT := $ARMGCC/
GNU_VERSION := $ARMGCC_VERSION
GNU_PREFIX := arm-none-eabi" > Makefile.windows


echo ""
echo "Install micro-ecc..."

cd ../../../external/micro-ecc

./build_all.sh


echo ""
echo "Libraries Installed!"