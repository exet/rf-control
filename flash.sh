#!/bin/bash  

# Stop script on error
set -e

zip_file=$1

mkdir -p tmp
unzip $zip_file -d tmp
cd tmp

# Flash all hex files
# It's very very bad :(
for hex_file in *.hex; do
	echo ""
	echo "Flashing $hex_file ..."
	
	nrfjprog -f nrf52 --recover
	nrfjprog -f nrf52 --eraseall
	nrfjprog -f nrf52 --program $hex_file
	nrfjprog -f nrf52 --reset
done

# Delete temporary directory
cd ..
rm -r tmp

echo ""
echo "Done!"

