/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @example examples/ble_peripheral/ble_app_hrs/main.c
 *
 * @brief Heart Rate Service Sample Application main file.
 *
 * This file contains the source code for a sample application using the Heart Rate service
 * (and also Battery and Device Information services). This application uses the
 * @ref srvlib_conn_params module.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_sdm.h"
#include "app_error.h"
#include "ble.h"
#include "ble_err.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_bas.h"
#include "ble_hrs.h"
#include "ble_dis.h"
#include "ble_dfu.h"
#include "ble_conn_params.h"
#include "sensorsim.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "fds.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_lesc.h"
#include "nrf_ble_qwr.h"
#include "ble_conn_state.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_queue.h"
#include "nrf_delay.h"
#include "app_scheduler.h"
#include "app_button.h"
#include "task_manager.h"
#include "nrf_drv_wdt.h"
#include "hardfault_genhf.h"

#include "ble_dvs.h"
#include "ble_das.h"

#include "gpio.h"
#include "spi_dpy.h"
#include "adc.h"



const static char FIRMWARE_VERSION[] = 
    #include "VERSION"
    #ifdef DEBUG
    "-d"
    #endif
    ;

const static char HARDWARE_VERSION[] = 
    "2.3"
    ;


#define DEVICE_NAME                         "RF-control"                                /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME                   "Club-electronic"                                /**< Manufacturer. Will be passed to Device Information Service. */

#define APP_ADV_INTERVAL                    MSEC_TO_UNITS(200, UNIT_0_625_MS)       /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_DURATION                    0                                       /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define APP_BLE_CONN_CFG_TAG                1                                       /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_OBSERVER_PRIO               3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define BATTERY_LEVEL_MEAS_INTERVAL         APP_TIMER_TICKS(1*2*1000)               /**< Battery level measurement interval (ticks). */
#define SESSION_MANAGER_CHECK_INTERVAL      APP_TIMER_TICKS(1*1*1000)               /**< Session manager check interval (ticks). */
#define MIN_BATTERY_LEVEL                   20                                      /**< Minimum simulated battery level. */
#define MAX_BATTERY_LEVEL                   100                                     /**< Maximum simulated 7battery level. */

#define MIN_CONN_INTERVAL                   MSEC_TO_UNITS(7.5, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.4 seconds). */
#define MAX_CONN_INTERVAL                   MSEC_TO_UNITS(7.5, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (0.65 second). */
#define SLAVE_LATENCY                       0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                    MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY      APP_TIMER_TICKS(5000)                   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY       APP_TIMER_TICKS(30000)                  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT        3                                       /**< Number of attempts before giving up the connection parameter negotiation. */

#define LESC_DEBUG_MODE                     0                                       /**< Set to 1 to use LESC debug keys, allows you to use a sniffer to inspect traffic. */

#define SEC_PARAM_BOND                      1                                       /**< Perform bonding. */
#define SEC_PARAM_MITM                      0                                       /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                      1                                       /**< LE Secure Connections enabled. */
#define SEC_PARAM_KEYPRESS                  0                                       /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES           BLE_GAP_IO_CAPS_NONE                    /**< No I/O capabilities. */
#define SEC_PARAM_OOB                       0                                       /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE              7                                       /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE              16                                      /**< Maximum encryption key size. */

#define DEAD_BEEF                           0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define SCHED_MAX_EVENT_DATA_SIZE           16
#define SCHED_QUEUE_SIZE                    32

#define ON_THE_BED_SENSE_TRH                6000

#define SLEEP_SESSION_MAX_TIME_SEC          (14*60*60)
#define ON_THE_BED_CHECK_PERIOD             (5*60*1000)

#define ADC_PERIOD_MS                       10

#define POSE_SAVE_PERIOD_SEC                (2*60)

#define USE_ANALOG_DISABLE                  0

#define SAADC_SENSE_MEAS_ACTIVE()     (adc_task_sense || adc_task_sense_service)
#define SAADC_VBAT_MEAS_ACTIVE()      (adc_task_vbat || adc_task_vbat_service)


#ifdef USE_BLE_DVS
BLE_DVS_DEF(m_dvs);
static char ble_hvx_buff[128];
static int8_t sistem_status[6] = {};

#endif

BLE_DAS_DEF(m_das);
BLE_BAS_DEF(m_bas);                                                 /**< Structure used to identify the battery service. */
NRF_BLE_GATT_DEF(m_gatt);                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                 /**< Advertising module instance. */
APP_TIMER_DEF(m_battery_timer_id);
APP_TIMER_DEF(update_status_timer_id);
APP_TIMER_DEF(led_timer_id);
APP_TIMER_DEF(scan_joystick_timer_id);
APP_TIMER_DEF(scan_buttons_timer_id);


static uint16_t m_conn_handle         = BLE_CONN_HANDLE_INVALID;    /**< Handle of the current connection. */
static ble_advdata_t advdata;
static ble_advdata_manuf_data_t  manuf_specific_data;
static sensorsim_cfg_t   m_battery_sim_cfg;                         /**< Battery Level sensor simulator configuration. */
static sensorsim_state_t m_battery_sim_state;                       /**< Battery Level sensor simulator state. */

static ble_uuid_t m_adv_uuids[] =                                   /**< Universally unique service identifiers. */
{
//    {BLE_UUID_BATTERY_SERVICE,              BLE_UUID_TYPE_BLE},
    {BLE_UUID_DEVICE_INFORMATION_SERVICE,   BLE_UUID_TYPE_BLE}
};

static uint32_t ble_protocol_rsp_notify(uint8_t *data, uint16_t len);
static void das_write_evt_handler(ble_das_t * p_das, ble_gatts_evt_write_t const * p_evt);
static void dvs_write_evt_handler(ble_dvs_t * p_dvs, ble_gatts_evt_write_t const * p_evt);
static void update_advertising_data();
static bool on_the_bed();
static void process_session_manager(void);

/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}


/**@brief Clear bond information from persistent storage.
 */
static void delete_bonds(void)
{
    ret_code_t err_code;

    NRF_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
void advertising_start(bool erase_bonds)
{
    ret_code_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void pm_evt_handler(pm_evt_t const * p_evt)
{
    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
        case PM_EVT_PEERS_DELETE_SUCCEEDED:
            advertising_start(false);
            break;

        default:
            break;
    }
}


/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *
 * @param[in] p_context  Pointer used for passing some arbitrary information (context) from the
 *                       app_start_timer() call to the timeout handler.
 */
static void battery_level_meas_timeout_handler(void * p_context)
{

    static uint8_t level = 100;
    UNUSED_PARAMETER(p_context);

    ble_bas_battery_level_update(&m_bas, level, NULL);

    if(--level < 20) 
        level = 100;

}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    ret_code_t err_code;

    // Initialize timer module.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create timers.

}


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

//    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_HEART_RATE_SENSOR);
//    APP_ERROR_CHECK(err_code);

//    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

//    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
//    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
//    gap_conn_params.slave_latency     = SLAVE_LATENCY;
//    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

//    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
//    APP_ERROR_CHECK(err_code);
}


/**@brief GATT module event handler.
 */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("GATT ATT MTU on connection 0x%x changed to %d.", p_evt->conn_handle, p_evt->params.att_mtu_effective);
        
    }
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);
}


static void advertising_config_get(ble_adv_modes_config_t * p_config)
{
    memset(p_config, 0, sizeof(ble_adv_modes_config_t));

    p_config->ble_adv_fast_enabled  = true;
    p_config->ble_adv_fast_interval = APP_ADV_INTERVAL;
    p_config->ble_adv_fast_timeout  = APP_ADV_DURATION;
}


static void disconnect(uint16_t conn_handle, void * p_context)
{
    UNUSED_PARAMETER(p_context);

    ret_code_t err_code = sd_ble_gap_disconnect(conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    if (err_code != NRF_SUCCESS)
    {
        NRF_LOG_WARNING("Failed to disconnect connection. Connection handle: %d Error: %d", conn_handle, err_code);
    }
    else
    {
        NRF_LOG_DEBUG("Disconnected connection handle %d", conn_handle);
    }
}

void ble_disconnect() {
    disconnect(m_conn_handle, NULL);
}


// YOUR_JOB: Update this code if you want to do anything given a DFU event (optional).
/**@brief Function for handling dfu events from the Buttonless Secure DFU service
 *
 * @param[in]   event   Event from the Buttonless Secure DFU service.
 */
static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
        {
            NRF_LOG_INFO("Device is preparing to enter bootloader mode.");

            // Prevent device from advertising on disconnect.
            ble_adv_modes_config_t config;
            advertising_config_get(&config);
            config.ble_adv_on_disconnect_disabled = true;
            ble_advertising_modes_config_set(&m_advertising, &config);

            // Disconnect all other bonded devices that currently are connected.
            // This is required to receive a service changed indication
            // on bootup after a successful (or aborted) Device Firmware Update.
            uint32_t conn_count = ble_conn_state_for_each_connected(disconnect, NULL);
            NRF_LOG_INFO("Disconnected %d links.", conn_count);
            break;
        }

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


static void das_write_evt_handler(ble_das_t * p_das, ble_gatts_evt_write_t const * p_evt)
{
    if (p_evt->handle == p_das->cmd_handles.value_handle) {
        // обработка команды
    }
    // Rsp Notification
    else if (p_evt->handle == p_das->rsp_handles.cccd_handle) {
        if (p_evt->len == 2) {
            if (ble_srv_is_notification_enabled(p_evt->data)) {
                NRF_LOG_INFO("rsp notification subscribed");
                //ble_protocol_on_rsp_notify(true);
//                board_led_on(LED_RED);
            }
            else {
                NRF_LOG_INFO("rsp notification unsubscribed");
//                board_led_off(LED_RED);
                //ble_protocol_on_rsp_notify(false);
            }
        }
    }
}

static uint32_t ble_protocol_rsp_write(uint8_t *data, uint16_t len) {
    return (int)ble_das_rsp_write(&m_das, data, len);
}

static uint32_t ble_protocol_rsp_notify(uint8_t *data, uint16_t len) {
    return ble_das_rsp_notify(&m_das, data, len);
}

/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void)
{
    ret_code_t         err_code;
    ble_bas_init_t     bas_init;
    ble_dis_init_t     dis_init;
    ble_das_init_t     das_init;
    nrf_ble_qwr_init_t qwr_init = {0};
    ble_dfu_buttonless_init_t dfus_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

    dfus_init.evt_handler = ble_dfu_evt_handler;
	
//    err_code = ble_dfu_buttonless_init(&dfus_init);
//    APP_ERROR_CHECK(err_code);

    // Initialize Battery Service.
    memset(&bas_init, 0, sizeof(bas_init));

    bas_init.evt_handler          = NULL;
    bas_init.support_notification = true;
    bas_init.p_report_ref         = NULL;
    bas_init.initial_batt_level   = 100;

    // Here the sec level for the Battery Service can be changed/increased.
    bas_init.bl_rd_sec        = SEC_OPEN;
    bas_init.bl_cccd_wr_sec   = SEC_OPEN;
    bas_init.bl_report_rd_sec = SEC_OPEN;

    err_code = ble_bas_init(&m_bas, &bas_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Data Service.
    memset(&das_init, 0, sizeof(das_init));

    das_init.write_evt_handler = das_write_evt_handler;
    das_init.access            = false;

    err_code = ble_das_init(&m_das, &das_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Device Information Service.
//    memset(&dis_init, 0, sizeof(dis_init));

//    ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *)MANUFACTURER_NAME);
//    ble_srv_ascii_to_utf8(&dis_init.fw_rev_str, (char *)FIRMWARE_VERSION);
//    ble_srv_ascii_to_utf8(&dis_init.hw_rev_str, (char *)HARDWARE_VERSION);

//    dis_init.dis_char_rd_sec = SEC_OPEN;

//    err_code = ble_dis_init(&dis_init);
//    APP_ERROR_CHECK(err_code);

    // Initialize Data Service.

}

static void battery_timer_create() {
    APP_ERROR_CHECK(app_timer_create(&m_battery_timer_id, APP_TIMER_MODE_REPEATED, battery_level_meas_timeout_handler));
}

/**@brief Function for starting application timers.
 */
static void application_timers_start(void)
{
    ret_code_t err_code;

    // Start application timers.
    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);

}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        NRF_LOG_DEBUG("BLE_CONN_PARAMS_EVT_FAILED");
//        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
//        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = m_das.rsp_handles.cccd_handle;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            break;

        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;

        default:
            break;
    }
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);

            {
                const ble_gap_conn_params_t *conn_params = &p_ble_evt->evt.gap_evt.params.connected.conn_params;
                NRF_LOG_DEBUG("min_conn_interval: %d (%d ms)", conn_params->min_conn_interval, (int)(1.25f * conn_params->min_conn_interval));
                NRF_LOG_DEBUG("max_conn_interval: %d (%d ms)", conn_params->max_conn_interval, (int)(1.25f * conn_params->min_conn_interval));
            }
            
            
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected, reason %d.",
                          p_ble_evt->evt.gap_evt.params.disconnected.reason);
            m_conn_handle = BLE_CONN_HANDLE_INVALID;

            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE:
            NRF_LOG_DEBUG("Connection parameters updated");

            {
                const ble_gap_conn_params_t *conn_params = &p_ble_evt->evt.gap_evt.params.connected.conn_params;
                NRF_LOG_DEBUG("min_conn_interval: %d (%d ms)", conn_params->min_conn_interval, (int)(1.25f * conn_params->min_conn_interval));
                NRF_LOG_DEBUG("max_conn_interval: %d (%d ms)", conn_params->max_conn_interval, (int)(1.25f * conn_params->min_conn_interval));
            }
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
    
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            NRF_LOG_DEBUG("BLE_GAP_EVT_SEC_PARAMS_REQUEST");
            break;
        
        case BLE_GAP_EVT_AUTH_KEY_REQUEST:
            NRF_LOG_INFO("BLE_GAP_EVT_AUTH_KEY_REQUEST");
            break;

        case BLE_GAP_EVT_LESC_DHKEY_REQUEST:
            NRF_LOG_INFO("BLE_GAP_EVT_LESC_DHKEY_REQUEST");
            break;

         case BLE_GAP_EVT_AUTH_STATUS:
             NRF_LOG_INFO("BLE_GAP_EVT_AUTH_STATUS: status=0x%x bond=0x%x lv4: %d kdist_own:0x%x kdist_peer:0x%x",
                          p_ble_evt->evt.gap_evt.params.auth_status.auth_status,
                          p_ble_evt->evt.gap_evt.params.auth_status.bonded,
                          p_ble_evt->evt.gap_evt.params.auth_status.sm1_levels.lv4,
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_own),
                          *((uint8_t *)&p_ble_evt->evt.gap_evt.params.auth_status.kdist_peer));
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}



/**@brief Function for the Peer Manager initialization.
 */
static void peer_manager_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t           err_code;

    err_code = pm_init();
    APP_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond           = SEC_PARAM_BOND;
    sec_param.mitm           = SEC_PARAM_MITM;
    sec_param.lesc           = SEC_PARAM_LESC;
    sec_param.keypress       = SEC_PARAM_KEYPRESS;
    sec_param.io_caps        = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob            = SEC_PARAM_OOB;
    sec_param.min_key_size   = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size   = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc  = 1;
    sec_param.kdist_own.id   = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id  = 1;

    err_code = pm_sec_params_set(&sec_param);
    APP_ERROR_CHECK(err_code);

    err_code = pm_register(pm_evt_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t                err_code;
    ble_advertising_init_t    init;
    ble_gap_addr_t            ble_gap_addr;
    
    err_code = sd_ble_gap_addr_get(&ble_gap_addr);
    APP_ERROR_CHECK(err_code);


    memset(&init, 0, sizeof(init));

    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;
//    init.advdata.include_appearance      = true;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_more_available.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_more_available.p_uuids  = m_adv_uuids;
    init.advdata.p_manuf_specific_data   = &manuf_specific_data;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
    APP_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);

    memcpy(&advdata, &init.advdata, sizeof(ble_advdata_t));
}

static uint32_t get_rtc_counter(void)
{
    return NRF_RTC1->COUNTER;
}

/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(get_rtc_counter);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}




/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_lesc_request_handler();
    APP_ERROR_CHECK(err_code);

    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

int reboot() {
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
        // На случай, если сейчас идет транзакция
        nrf_delay_ms(100);

        disconnect(m_conn_handle, NULL);

        while (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
            idle_state_handle();
        }
    }

    sd_power_system_off();

    NVIC_SystemReset();

    return 0;
}

/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

static void led_timer_handler(void * p_context) {

    static uint8_t led_num = 0;

    all_leds_off();
    nrf_gpio_pin_write(LEDS_NUM[led_num], LED_ON);
//    NRF_LOG_INFO("Led = %d", led_num);

    led_num ++;
    if(led_num >= COUNT_LEDS_NUM) {
        led_num = 0;
    }

}

static void led_timer_create() {
    APP_ERROR_CHECK(app_timer_create(&led_timer_id, APP_TIMER_MODE_REPEATED, led_timer_handler));
}

static void led_timer_start(uint32_t ms) {
    APP_ERROR_CHECK(app_timer_start(led_timer_id, APP_TIMER_TICKS(ms), NULL));
}

static void led_timer_stop() {
    APP_ERROR_CHECK(app_timer_stop(led_timer_id));
}

static int16_t x1 = 0; 
static int16_t y1 = 0; 
static int16_t x2 = 0; 
static int16_t y2 = 0;

static void scan_joystick_timer_handler(void * p_context) {

    oled_print_set_cursor(1, 1);
    oled_print_int(((int32_t)(adc_data[0]/16)),3);

    oled_print_set_cursor(1, 5);
    oled_print_int(((int32_t)(adc_data[1]/16)),3);

    oled_print_set_cursor(1, 9);
    oled_print_int(((int32_t)(adc_data[2]/16)),3);

    oled_print_set_cursor(1, 13);
    oled_print_int(((int32_t)(adc_data[3]/16)),3);

    const uint8_t J1_X0 = 7 + 25;
    const uint8_t J1_Y0 = 10 + 25;
    const uint8_t J2_X0 = 71 + 25;
    const uint8_t J2_Y0 = 10 + 25;

    oled_clear_pixel(x1, y1);
    oled_clear_pixel(x2, y2);

    x1 = (int16_t)(((int32_t)(adc_data[1]) * 50) / 15200) + J1_X0;
    y1 = (int16_t)(((int32_t)(-adc_data[0]) * 50) / 15200) + J1_Y0;
    x2 = (int16_t)(((int32_t)(adc_data[3]) * 50) / 15200) + J2_X0;
    y2 = (int16_t)(((int32_t)(-adc_data[2]) * 50) / 15200) + J2_Y0;

    oled_print_pixel(x1, y1);
    oled_print_pixel(x2, y2);

}

static void scan_joystick_timer_create() {
    APP_ERROR_CHECK(app_timer_create(&scan_joystick_timer_id, APP_TIMER_MODE_REPEATED, scan_joystick_timer_handler));
}

static void scan_joystick_timer_start(uint32_t ms) {
    APP_ERROR_CHECK(app_timer_start(scan_joystick_timer_id, APP_TIMER_TICKS(ms), NULL));
}

static void scan_joystick_timer_stop() {
    APP_ERROR_CHECK(app_timer_stop(scan_joystick_timer_id));
}


static void scan_buttons_timer_handler(void * p_context) {

    for(int i = 0; i < COUNT_BUTS_NUM; i ++) {
        if( nrf_gpio_pin_read(BUTS_NUM[i]) == 0) 
            sistem_status[4] |= (1 << i);
        else 
            sistem_status[4] &=~(1 << i);
    }

    for(int i = 0; i < COUNT_LEDS_NUM; i ++) {
        if( nrf_gpio_pin_out_read(LEDS_NUM[i]) == 0) 
            sistem_status[5] |= (1 << i);
        else 
            sistem_status[5] &=~(1 << i);
    }

    for(int i = 0; i < 4; i ++) {
        sistem_status[i] = (int8_t)((int32_t)(adc_data[i])/(1<<(14-8)));
    }

    ble_protocol_rsp_notify(sistem_status, sizeof(sistem_status));
}

static void scan_buttons_timer_create() {
    APP_ERROR_CHECK(app_timer_create(&scan_buttons_timer_id, APP_TIMER_MODE_REPEATED, scan_buttons_timer_handler));
}

static void scan_buttons_timer_start(uint32_t ms) {
    APP_ERROR_CHECK(app_timer_start(scan_buttons_timer_id, APP_TIMER_TICKS(ms), NULL));
}

static void scan_buttons_timer_stop() {
    APP_ERROR_CHECK(app_timer_stop(scan_buttons_timer_id));
}

/**@brief Function for application main entry.
 */
int main(void)
{
    bool erase_bonds;
    // Initialize.
    log_init();
    timers_init();

    // Инициализируем время пораньше, чтобы скорее стартовать отсчет

    power_management_init();
    ble_stack_init();
    gap_params_init();
    gatt_init();
    advertising_init();
    services_init();
    conn_params_init();
    peer_manager_init();
    battery_timer_create();

    my_gpio_init();

    led_timer_create();
    led_timer_start(300);

    scan_joystick_timer_create();
    scan_buttons_timer_create();
    
    oled_init();

    int8_t n = 0;
    oled_print_set_cursor(1, 0);
    oled_print_int(n,3);
    oled_print_set_cursor(1, 4);
    oled_print_int(n,3);
    oled_print_set_cursor(1, 9);
    oled_print_int(n,3);
    oled_print_set_cursor(1, 13);
    oled_print_int(n,3);

    adc_init();

    scan_joystick_timer_start(100);
    scan_buttons_timer_start(20);

    // Start execution.
    NRF_LOG_INFO("RF control started.");
    application_timers_start();
    advertising_start(erase_bonds);
    // Enter main loop
    for (;;)
    {
        idle_state_handle();
        oled_screen_update();
    }
}


