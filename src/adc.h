#ifndef _ADC_H_
#define _ADC_H_

#include "kalman.h"

void adc_init(void);

struct adc_struct 
{
    float filter;
    float val;
};

extern float adc_data[];


#endif // _ADC_H_