#ifndef SPI_DPY_H__
#define SPI_DPY_H__

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

#define DPY_BUFFER_SIZE       1024
#define DPY_Y_LINES           128
#define DPY_X_LINES           64

typedef enum
{
  NO_ERROR_FLASH = 0,
  ERROR_BUSE_FLASH,
  ERROR_WRITE_ENABLE,
  ERROR_WRITE_DISABLE,
  ERROR_WRITE_STATUS_REG,
  ERROR_READ_DATA_FLASH
} return_flash_status_error;

extern uint8_t Type_flash;

void oled_init(void);
void oled_screen_update(void);
void oled_print(char * str);
void oled_print_set_cursor(uint8_t cursor_y, uint8_t cursor_x);
void oled_print_int(int32_t val, uint8_t size);
void oled_print_pixel(uint8_t x, uint8_t y);
void oled_clear_pixel(uint8_t x, uint8_t y);

#ifdef __cplusplus
}
#endif

#endif // SPI_DPY_H__