#ifndef _GPIO_H_
#define _GPIO_H_

#include <stdint.h>
#include "nrf_error.h"
#include "nrf_gpio.h"

#define BUT_1               NRF_GPIO_PIN_MAP(0, 19)
#define BUT_2               NRF_GPIO_PIN_MAP(0, 25)
#define BUT_3               NRF_GPIO_PIN_MAP(0, 9)
#define BUT_4               NRF_GPIO_PIN_MAP(0, 10)
#define BUT_5               NRF_GPIO_PIN_MAP(0, 18)

#define LED_VD1             NRF_GPIO_PIN_MAP(0, 17)
#define LED_VD2             NRF_GPIO_PIN_MAP(0, 26)
#define LED_VD3             NRF_GPIO_PIN_MAP(0, 27)
#define LED_R_VD4           NRF_GPIO_PIN_MAP(0, 24)
#define LED_G_VD4           NRF_GPIO_PIN_MAP(0, 23)
#define LED_B_VD4           NRF_GPIO_PIN_MAP(0, 22)

#define ADC_RX1_PIN         NRF_SAADC_INPUT_AIN4    // P0.28
#define ADC_RY1_PIN         NRF_SAADC_INPUT_AIN5    // P0.29
#define ADC_RX2_PIN         NRF_SAADC_INPUT_AIN6    // P0.30
#define ADC_RY2_PIN         NRF_SAADC_INPUT_AIN7    // P0.31

#define SPI_DPY_CS_PIN      NRF_GPIO_PIN_MAP(0, 4)
#define SPI_DPY_DC_PIN      NRF_GPIO_PIN_MAP(0, 5)
#define SPI_DPY_RS_PIN      NRF_GPIO_PIN_MAP(0, 6)
#define SPI_DPY_MOSI_PIN    NRF_GPIO_PIN_MAP(0, 7)
#define SPI_DPY_SCK_PIN     NRF_GPIO_PIN_MAP(0, 8)

#define SPI_DPY_DATA        nrf_gpio_pin_write(SPI_DPY_DC_PIN, 1)
#define SPI_DPY_COMMAND     nrf_gpio_pin_write(SPI_DPY_DC_PIN, 0)
#define SPI_DPY_SET         nrf_gpio_pin_write(SPI_DPY_RS_PIN, 1)
#define SPI_DPY_RESET       nrf_gpio_pin_write(SPI_DPY_RS_PIN, 0)

extern uint8_t LEDS_NUM[];
extern const uint8_t COUNT_LEDS_NUM;

extern uint8_t BUTS_NUM[];
extern const uint8_t COUNT_BUTS_NUM;

#define LED_ON_STATE 0

#if LED_ON_STATE == 1
#define LED_ON   1
#define LED_OFF  0
#else
#define LED_ON   0
#define LED_OFF  1
#endif

ret_code_t my_gpio_init(void);
void all_leds_off(void);

#endif // _GPIO_H_