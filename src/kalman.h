#ifndef _KALMAN_H_
#define _KALMAN_H_

#include <stdint.h>

typedef struct {
    float factor;
    float val;
} kalman_config_t;

float kalman(kalman_config_t * config, const float val);

#endif // _KALMAN_H_