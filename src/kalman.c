#include "kalman.h"

float kalman(kalman_config_t * config, const float val) {
    float new_val = (config->val * (config->factor - 1) + val) / config->factor;

    config->val = new_val;

    return new_val;
}