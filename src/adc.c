#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "nrf.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "app_error.h"

#include "nrf_log.h"

#include "gpio.h"
#include "adc.h"

#define ADC_PERIOD_MS         5
#define CHANNELS_PER_SAMPLE   4

static const float N_MEAN_FILTER_ADC = 100;
static const float MEAN_VALUE_ADC = 7600.0;
static const float MEAN_DELDA_ADC = 400;
float adc_data_mean[CHANNELS_PER_SAMPLE] = {MEAN_VALUE_ADC, MEAN_VALUE_ADC, MEAN_VALUE_ADC, MEAN_VALUE_ADC};
static const float N_FILTER_ADC = 5;
float adc_data[CHANNELS_PER_SAMPLE] = {0.0, 0.0, 0.0, 0.0};

#define SAMPLES_IN_BUFFER     (1 * CHANNELS_PER_SAMPLE)

static const nrfx_timer_t m_timer = NRFX_TIMER_INSTANCE(1);
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER];
static nrf_ppi_channel_t     m_ppi_channel;
static uint32_t              m_adc_evt_counter;

static volatile uint8_t saadc_r_cnt = 0;

static float abs(float val) {
    if( val < 0 ) 
        val = -val;
    return val;
}

static void timer_handler(nrf_timer_event_t event_type, void * p_context)
{

}

static void saadc_sampling_event_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    nrfx_timer_config_t timer_cfg = NRFX_TIMER_DEFAULT_CONFIG;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
    err_code = nrfx_timer_init(&m_timer, &timer_cfg, timer_handler);
    APP_ERROR_CHECK(err_code);

    /* setup m_timer for compare event every 25ms */
    uint32_t ticks = nrfx_timer_ms_to_ticks(&m_timer, ADC_PERIOD_MS);
    nrfx_timer_extended_compare(&m_timer,
                                   NRF_TIMER_CC_CHANNEL0,
                                   ticks,
                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                   false);
    nrfx_timer_enable(&m_timer);

    uint32_t timer_compare_event_addr = nrfx_timer_compare_event_address_get(&m_timer, NRF_TIMER_CC_CHANNEL0);
    uint32_t saadc_sample_task_addr   = nrfx_saadc_sample_task_get();

    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
    err_code = nrfx_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_ppi_channel_assign(m_ppi_channel,
                                          timer_compare_event_addr,
                                          saadc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
}


static void saadc_sampling_event_enable(void)
{
    ret_code_t err_code = nrfx_ppi_channel_enable(m_ppi_channel);
    APP_ERROR_CHECK(err_code);
}

static void saadc_sampling_event_disable(void)
{
    ret_code_t err_code = nrfx_ppi_channel_disable(m_ppi_channel);
    APP_ERROR_CHECK(err_code);
}


static void saadc_callback(nrfx_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrfx_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        static int i = 0;

        float data_adc_new = (float)p_event->data.done.p_buffer[i];

        kalman_config_t adc_data_current;
        adc_data_current.factor = N_FILTER_ADC;
        adc_data_current.val = adc_data[i];

        adc_data[i] = kalman(&adc_data_current, (data_adc_new - adc_data_mean[i]));
        if(abs(data_adc_new - MEAN_VALUE_ADC) < MEAN_DELDA_ADC) {
            adc_data_current.factor = N_MEAN_FILTER_ADC;
            adc_data_current.val = adc_data_mean[i];
            adc_data_mean[i] = kalman(&adc_data_current, data_adc_new);
        }
        i ++;
        if (i >= SAMPLES_IN_BUFFER) i = 0;


        m_adc_evt_counter++;
    }
}


static void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
    NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(ADC_RX1_PIN);

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    channel_config.pin_p = ADC_RX1_PIN;
    err_code = nrfx_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);

    channel_config.pin_p = ADC_RY1_PIN;
    err_code = nrfx_saadc_channel_init(1, &channel_config);
    APP_ERROR_CHECK(err_code);

    channel_config.pin_p = ADC_RX2_PIN;
    err_code = nrfx_saadc_channel_init(2, &channel_config);
    APP_ERROR_CHECK(err_code);

    channel_config.pin_p = ADC_RY2_PIN;
    err_code = nrfx_saadc_channel_init(3, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    err_code = nrfx_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

}

void adc_init(void) 
{
    saadc_init();
    saadc_sampling_event_init();
    saadc_sampling_event_enable();
}