#include "spi_dpy.h"
#include "nrfx_spim.h"
#include "nrf_delay.h"
#include "main.h"
#include "nrf_log.h"
#include "gpio.h"
#include "app_timer.h"
#include "symbols_8x8.h"

#include <string.h>

#define SPI_INSTANCE  0                                           /**< SPI instance index. */
static const nrfx_spim_t spi = NRFX_SPIM_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */
static volatile bool spi_xfer_done = true;  /**< Flag used to indicate that SPI instance completed the transfer. */

APP_TIMER_DEF(oled_timer_id);

uint8_t displayBuff[DPY_BUFFER_SIZE] = {0};

#define LENGTH_BUFFERS  255
static uint8_t m_tx_buf[LENGTH_BUFFERS] = {0x00};         /**< TX buffer. */
static uint8_t m_rx_buf[LENGTH_BUFFERS + 10] = {0x00};    /**< RX buffer. */

static const uint8_t m_length = sizeof(m_tx_buf);         /**< Transfer length. */

static bool flag_screen_need_update = false;

static uint8_t cursor = 0;

#define MAX_NUMBER_FLASH_FAIL   12
#define STEP_TIME_WAIT_MS       100

#define MAX_READ_BYTE_FROM_SPI  251
#define MAX_WRITE_BYTE_TO_SPI   251

#define LENGTH_READ_COMMAND     4
#define LENGTH_WRITE_COMMAND    4

/**
* @brief SPI user event handler.
* @param event
*/
void spim_event_handler(nrfx_spim_evt_t const * p_event,
                       void *                  p_context)
{
    spi_xfer_done = true;
}

static void SPI_Ext_FLASH_WriteRead( uint8_t const * p_tx_buffer,
                                    uint8_t         tx_buffer_length,
                                    uint8_t       * p_rx_buffer,
                                    uint8_t         rx_buffer_length)
{
  while (!spi_xfer_done)
  {
    __WFE();
  }

  spi_xfer_done = false;
  memset(m_tx_buf, 0, LENGTH_BUFFERS);
  memset(m_rx_buf, 0, LENGTH_BUFFERS);
  memcpy(m_tx_buf, p_tx_buffer, tx_buffer_length);

  if ((rx_buffer_length != NULL) && (p_rx_buffer != NULL))
  {
    nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, tx_buffer_length + rx_buffer_length, m_rx_buf, tx_buffer_length + rx_buffer_length);
    APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0));

    memcpy(p_rx_buffer, m_rx_buf + tx_buffer_length, rx_buffer_length);
    __asm("NOP");
  }
  else
  {
    nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, tx_buffer_length, m_rx_buf, 0);
    APP_ERROR_CHECK(nrfx_spim_xfer(&spi, &xfer_desc, 0));
  }

  while (!spi_xfer_done)
  {
    __WFE();
  }

  if ((rx_buffer_length != NULL) && (p_rx_buffer != NULL))
  {
    memcpy(p_rx_buffer, m_rx_buf + tx_buffer_length, rx_buffer_length);
  }
}

void SPI_WriteData(uint8_t spi_tx_val)
{
  SPI_DPY_DATA;
  uint8_t data[1] = {spi_tx_val};
  SPI_Ext_FLASH_WriteRead(data, 1, NULL, NULL);
  nrf_delay_us(1);
}

void SPI_WriteCommand(uint8_t spi_tx_val)
{
  SPI_DPY_COMMAND;
  uint8_t data[1] = {spi_tx_val};
  SPI_Ext_FLASH_WriteRead(data, 1, NULL, NULL);
  nrf_delay_ms(1);
}

void ssd1306_write(uint8_t * p_tx_buffer, uint8_t tx_buffer_length)
{
  SPI_DPY_DATA;
  SPI_Ext_FLASH_WriteRead(p_tx_buffer, tx_buffer_length, NULL, NULL);
  nrf_delay_ms(1);
}

void oled_screen_update(void) {

    if(flag_screen_need_update == true) {
          
        for(uint16_t i=0; i<DPY_BUFFER_SIZE; i++)
        {
//            displayBuff[i] = Symbols8x8[i];
            SPI_WriteData(displayBuff[i]);
        }
        flag_screen_need_update = false;
//        NRF_LOG_INFO("screen update");
    }
}


static void oled_timer_handler(void * p_context) {
    
}

static void oled_timer_create() {
    APP_ERROR_CHECK(app_timer_create(&oled_timer_id, APP_TIMER_MODE_REPEATED, oled_timer_handler));
}

static void oled_timer_start(uint32_t ms) {
    APP_ERROR_CHECK(app_timer_start(oled_timer_id, APP_TIMER_TICKS(ms), NULL));
}

static void oled_timer_stop() {
    APP_ERROR_CHECK(app_timer_stop(oled_timer_id));
}


static void SPI_DPY_Init(void)
{

  nrf_delay_ms(100);

  nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(m_tx_buf, LENGTH_BUFFERS, m_rx_buf, LENGTH_BUFFERS);

  nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;
  spi_config.frequency      = NRF_SPIM_FREQ_1M;
  spi_config.ss_pin         = SPI_DPY_CS_PIN;
  spi_config.miso_pin       = NRF_GPIO_PIN_MAP(0, 25);
  spi_config.mosi_pin       = SPI_DPY_MOSI_PIN;
  spi_config.sck_pin        = SPI_DPY_SCK_PIN;
  spi_config.ss_active_high = false;

  APP_ERROR_CHECK(nrfx_spim_init(&spi, &spi_config, spim_event_handler, NULL));

  nrf_delay_ms(10);

}



void oled_put_char(char str) {
    
    if( str == '\n') {
        uint8_t n_lines = DPY_Y_LINES / COLUMNS_SYMBOL;
        cursor += (n_lines - (cursor % n_lines)) ;
    }
    else {
        for(uint8_t i = 0; i < COLUMNS_SYMBOL; i++)  {
            displayBuff[cursor * COLUMNS_SYMBOL + i] = 
            Symbols8x8[(uint8_t)(str - ' ') * COLUMNS_SYMBOL + i];
        }
        cursor ++;
    }
    if (cursor >= DPY_BUFFER_SIZE / COLUMNS_SYMBOL)
    cursor = 0;
//    NRF_LOG_INFO("%d ", str);

}


void oled_print(char * str) {

    while (*str != 0)
        oled_put_char(*str++);

    flag_screen_need_update = true;

}

void oled_print_set_cursor(uint8_t cursor_y, uint8_t cursor_x)
{
    cursor = (cursor_y - 1) * (DPY_Y_LINES / COLUMNS_SYMBOL) + (cursor_x - 1);
}
        
void oled_print_int(int32_t val, uint8_t size) 
{   
    char buff[16] = {0};
    bool flag_minus = false;
    for(uint8_t i = 0; i < size; i++)
        buff[i] = ' ';

    if(val < 0)
    {
        val = -val;
        flag_minus = true;
    }
    for(uint8_t i = 0; i < size; i++)
    {
        buff[size - i] = (val % 10) + '0';
        val /= 10;
        if(val == 0)
        {
            if(flag_minus) {
                buff[size - i - 1] = '-';
            }

            break;
        }
    }

    oled_print(buff);
}

void oled_print_pixel(uint8_t x, uint8_t y) 
{
    uint16_t n_byte = (y/8)*128 + x;
    uint8_t n_bit = y%8;

    displayBuff[n_byte] |= (1 << n_bit);
}

void oled_clear_pixel(uint8_t x, uint8_t y) 
{
    uint16_t n_byte = (y/8)*128 + x;
    uint8_t n_bit = y%8;

    displayBuff[n_byte] &= ~(1 << n_bit);
}


void oled_init(void)  //ssd1306
{

  SPI_DPY_Init();

  //Сброс экрана и очистка буфера
  SPI_DPY_RESET;
  nrf_delay_ms(120);
  SPI_DPY_SET;
	
  SPI_WriteCommand(0xAE); //display off
  SPI_WriteCommand(0xD5); //Set Memory Addressing Mode
  SPI_WriteCommand(0x80); //00,Horizontal Addressing Mode;01,Vertical
  SPI_WriteCommand(0xA8); //Set Page Start Address for Page Addressing
  SPI_WriteCommand(0x3F); //Set COM Output Scan Direction
  SPI_WriteCommand(0xD3); //set low column address
  SPI_WriteCommand(0x00); //set high column address
  SPI_WriteCommand(0x40); //set start line address
  SPI_WriteCommand(0x8D); //set contrast control register
  SPI_WriteCommand(0x14);
  SPI_WriteCommand(0x20); //set segment re-map 0 to 127
  SPI_WriteCommand(0x00); //set normal display
  SPI_WriteCommand(0xA1); //set multiplex ratio(1 to 64)
  SPI_WriteCommand(0xC8); //
  SPI_WriteCommand(0xDA); //0xa4,Output follows RAM
  SPI_WriteCommand(0x12); //set display offset
  SPI_WriteCommand(0x81); //not offset
  SPI_WriteCommand(0x8F); //set display clock divide ratio/oscillator frequency
  SPI_WriteCommand(0xD9); //set divide ratio
  SPI_WriteCommand(0xF1); //set pre-charge period
  SPI_WriteCommand(0xDB);
  SPI_WriteCommand(0x40); //set com pins hardware configuration
  SPI_WriteCommand(0xA4);
  SPI_WriteCommand(0xA6); //set vcomh
  SPI_WriteCommand(0xAF); //0x20,0.77xVcc

  for(uint16_t i=0;i<DPY_BUFFER_SIZE;i++)
  {
    displayBuff[i]=0;
  }

  oled_timer_create();
  oled_timer_start(10);

  flag_screen_need_update = true;

}



