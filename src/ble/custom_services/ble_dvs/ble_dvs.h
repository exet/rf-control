#ifndef BLE_DVS_H__
#define BLE_DVS_H__

#define USE_BLE_DVS
#ifdef USE_BLE_DVS

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_DVS_BLE_OBSERVER_PRIO              2


/**@brief   Macro for defining a ble_dvs instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_DVS_DEF(_name)                                                                          \
static ble_dvs_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_DVS_BLE_OBSERVER_PRIO,                                                     \
                     ble_dvs_on_ble_evt, &_name)


// Forward declaration of the ble_dvs_t type.
typedef struct ble_dvs_s ble_dvs_t;

typedef enum
{
    BLE_DVS_EVT_NOTIFICATION_ENABLED,   /**< Value notification enabled event. */
    BLE_DVS_EVT_NOTIFICATION_DISABLED   /**< Value notification disabled event. */
} ble_dvs_evt_type_t;

/**@brief Data Service event handler type. */
typedef void (*ble_dvs_write_evt_handler_t) (ble_dvs_t * p_dvs, ble_gatts_evt_write_t const * p_evt);

/**@brief Data Service init structure. This contains all options and data needed for
 *        initialization of the service. */
typedef struct
{
    ble_dvs_write_evt_handler_t  write_evt_handler;                                    /**< Event handler to be called for handling events in the Data Service. */
    bool                         access;
} ble_dvs_init_t;

/**@brief Data Service structure. This contains various status information for the service. */
struct ble_dvs_s
{
    ble_dvs_write_evt_handler_t  write_evt_handler;  
    uint16_t                     service_handle;                                       /**< Handle of Data Service (as provided by the BLE stack). */
    uint16_t                     conn_handle;                                          /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    ble_gatts_char_handles_t     txt_handles;                                          /**< Handles related to the Command characteristic. */
    uint8_t                      max_len;                                              /**< Current maximum length, adjusted according to the current ATT MTU. */
    bool                         is_notification_enabled;
};


/**@brief Function for initializing the Data Service.
 *
 * @param[out]  p_dvs       Data Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_dvs_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_dvs_init(ble_dvs_t * p_dvs, ble_dvs_init_t const * p_dvs_init);


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Data Service.
 *
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 * @param[in]   p_context   Data Service structure.
 */
void ble_dvs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

uint32_t ble_dvs_txt_send(ble_dvs_t * p_dvs, char *str);

//uint32_t ble_dvs_rsp_send(ble_dvs_t * p_dvs, uint8_t rsp);

#ifdef __cplusplus
}
#endif

#endif // USE_BLE_DVS

#endif // BLE_DVS_H__

