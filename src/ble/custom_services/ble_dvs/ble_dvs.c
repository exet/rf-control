
#include "ble_dvs.h"

#ifdef USE_BLE_DVS

#include "sdk_common.h"
#include <string.h>
#include "ble_srv_common.h"
#include "nrf_log.h"


#ifndef OPCODE_LENGTH
#define OPCODE_LENGTH 1                                                              /**< Length of opcode inside Control Measurement packet. */
#endif
#ifndef HANDLE_LENGTH
#define HANDLE_LENGTH 2                                                              /**< Length of handle inside Control Measurement packet. */
#endif
#define MAX_DVS_LEN      /*20*/(NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum size of a transmitted Control Measurement. */

/// @todo UUID из sibur-ibeacon. Сгенерировать новый
#define CUSTOM_UUID_BASE                {0xA5, 0xE4, 0x87, 0x4D, 0x2C, 0x67, 0xD0, 0xB9, 0x41, 0x49, 0x19, 0x71, 0x00, 0x00, 0x00, 0x00}

#define DVS_UUID_BASE                   CUSTOM_UUID_BASE
#define DVS_UUID_SERVICE		0xE200

#define TXT_UUID_CHAR		        0xE201
#define TXT_UUID_CHAR_LEN               MAX_DVS_LEN


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_dvs       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_dvs_t * p_dvs, ble_evt_t const * p_ble_evt)
{
    p_dvs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_dvs       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_dvs_t * p_dvs, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_dvs->conn_handle = BLE_CONN_HANDLE_INVALID;
    p_dvs->is_notification_enabled = false;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_dvs       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_dvs_t * p_dvs, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if ((p_evt_write->handle == p_dvs->txt_handles.cccd_handle) &&
    (p_evt_write->len == 2))
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data)) {
            p_dvs->is_notification_enabled = true;
        }
        else {
            p_dvs->is_notification_enabled = false;
        }
    }

    if ((p_evt_write->len > 0) && (p_dvs->write_evt_handler != NULL)) {
        p_dvs->write_evt_handler(p_dvs, p_evt_write);       
    }
}


void ble_dvs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_dvs_t * p_dvs = (ble_dvs_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_dvs, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_dvs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_dvs, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_dvs_init(ble_dvs_t * p_dvs, const ble_dvs_init_t * p_dvs_init)
{
    ble_uuid_t ble_uuid;
    ble_uuid128_t base_uuid = {DVS_UUID_BASE};
    ble_uuid.uuid = DVS_UUID_SERVICE;
    p_dvs->conn_handle = BLE_CONN_HANDLE_INVALID;

    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &ble_uuid.type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_add_char_params_t add_char_params;

    // Initialize service structure
    p_dvs->write_evt_handler           = p_dvs_init->write_evt_handler;
    p_dvs->conn_handle                 = BLE_CONN_HANDLE_INVALID;
    p_dvs->max_len                     = MAX_DVS_LEN;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_dvs->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Txt characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.is_var_len        = true;
    add_char_params.uuid              = TXT_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = TXT_UUID_CHAR_LEN;
    add_char_params.init_len          = TXT_UUID_CHAR_LEN;
//    add_char_params.char_props.write  = 1;
//    add_char_params.char_props.read   = 1;
    add_char_params.char_props.notify = 1;
//    add_char_params.write_access      = SEC_OPEN;
//    add_char_params.read_access       = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;

    err_code = characteristic_add(p_dvs->service_handle, &add_char_params, &(p_dvs->txt_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}


uint32_t ble_dvs_txt_send(ble_dvs_t * p_dvs, char *str)
{
    uint32_t err_code;

    // Send value if connected and notifying
    if (p_dvs->conn_handle != BLE_CONN_HANDLE_INVALID && p_dvs->is_notification_enabled) {                
        uint16_t hvx_len = strlen(str);
        ble_gatts_hvx_params_t hvx_params;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_dvs->txt_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = (uint8_t *)str;

        err_code = sd_ble_gatts_hvx(p_dvs->conn_handle, &hvx_params);
        if (err_code != NRF_SUCCESS/* && err_code != NRF_ERROR_RESOURCES*/) {
            //err_code = NRF_ERROR_DATA_SIZE;
            NRF_LOG_ERROR("ble_dvs_txt_send error: %d", err_code);
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;
}

#endif // USE_BLE_DVS


