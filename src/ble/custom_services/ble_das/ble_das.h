#ifndef BLE_DAS_H__
#define BLE_DAS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_DAS_BLE_OBSERVER_PRIO              2


/**@brief   Macro for defining a ble_das instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_DAS_DEF(_name)                          \
static ble_das_t _name;                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                 \
                     BLE_DAS_BLE_OBSERVER_PRIO,     \
                     ble_das_on_ble_evt, &_name)


// Forward declaration of the ble_das_t type.
typedef struct ble_das_s ble_das_t;

/**@brief Data Service event handler type. */
typedef void (*ble_das_write_evt_handler_t) (ble_das_t * p_das, ble_gatts_evt_write_t const * p_evt);

/**@brief Data Service init structure. This contains all options and data needed for
 *        initialization of the service. */
typedef struct {
    ble_das_write_evt_handler_t  write_evt_handler;                                    /**< Event handler to be called for handling events in the Data Service. */
    bool                         access;
} ble_das_init_t;

/**@brief Data Service structure. This contains various status information for the service. */
struct ble_das_s {
    ble_das_write_evt_handler_t  write_evt_handler;  
    uint16_t                     service_handle;                                       /**< Handle of Data Service (as provided by the BLE stack). */
    uint16_t                     conn_handle;                                          /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    ble_gatts_char_handles_t     cmd_handles;                                          /**< Handles related to the Command characteristic. */
    ble_gatts_char_handles_t     rsp_handles;                                          /**< Handles related to the Response characteristic. */
    uint8_t                      max_len;                                              /**< Current maximum length, adjusted according to the current ATT MTU. */
    bool                         is_notification_enabled;
};


/**@brief Function for initializing the Data Service.
 *
 * @param[out]  p_das       Data Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_das_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_das_init(ble_das_t * p_das, ble_das_init_t const * p_das_init);


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Data Service.
 *
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 * @param[in]   p_context   Data Service structure.
 */
void ble_das_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

//uint32_t ble_das_txt_send(ble_das_t * p_das, char *str);

uint32_t ble_das_rsp_notify(ble_das_t *p_das, uint8_t *data, uint16_t len);
uint32_t ble_das_rsp_write(ble_das_t *p_das, uint8_t *data, uint16_t len);

#ifdef __cplusplus
}
#endif

#endif // BLE_DAS_H__

