
#include "sdk_common.h"
#include "ble_das.h"
#include <string.h>
#include "ble_srv_common.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#ifndef OPCODE_LENGTH
#define OPCODE_LENGTH 1                                                              /**< Length of opcode inside Control Measurement packet. */
#endif
#ifndef HANDLE_LENGTH
#define HANDLE_LENGTH 2                                                              /**< Length of handle inside Control Measurement packet. */
#endif
#define MAX_DAS_LEN      /*20*/(NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum size of a transmitted Control Measurement. */

/// @todo UUID из sibur-ibeacon. Сгенерировать новый
#define CUSTOM_UUID_BASE                {0xBA, 0xB4, 0x25, 0xEA, 0x51, 0xF0, 0x20, 0x9E, 0xC4, 0x4D, 0x07, 0x78, 0x00}

#define DAS_UUID_BASE                   CUSTOM_UUID_BASE
#define DAS_UUID_SERVICE		0xE100

#define CMD_UUID_CHAR		        0xE101
#define RSP_UUID_CHAR		        0xE102
//#define TXT_UUID_CHAR_LEN               MAX_DAS_LEN


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_das       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_das_t * p_das, ble_evt_t const * p_ble_evt)
{
    p_das->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_das       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_das_t * p_das, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_das->conn_handle = BLE_CONN_HANDLE_INVALID;
    p_das->is_notification_enabled = false;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_das       Data Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_das_t * p_das, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if ((p_evt_write->handle == p_das->rsp_handles.cccd_handle) &&
    (p_evt_write->len == 2))
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            p_das->is_notification_enabled = true;
            // NRF_LOG_DEBUG("p_das->is_notification_enabled = true;");
        }
        else
        {
            p_das->is_notification_enabled = false;
            // NRF_LOG_DEBUG("p_das->is_notification_enabled = false;");
        }
    }

    if ((p_evt_write->len > 0) && (p_das->write_evt_handler != NULL)) {
        p_das->write_evt_handler(p_das, p_evt_write);       
    }
}

void on_exchange_mtu_rsp(ble_das_t * p_das, ble_evt_t const * p_ble_evt) {
    ble_gattc_evt_exchange_mtu_rsp_t const * evt_exchange_mtu_rsp = &p_ble_evt->evt.gattc_evt.params.exchange_mtu_rsp;

    p_das->max_len = evt_exchange_mtu_rsp->server_rx_mtu - OPCODE_LENGTH - HANDLE_LENGTH;

    NRF_LOG_DEBUG("on_exchange_mtu_rsp. max_len = %d", p_das->max_len);
}


void ble_das_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_das_t * p_das = (ble_das_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_das, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_das, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_das, p_ble_evt);
            break;
        
        case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
            on_exchange_mtu_rsp(p_das, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_das_init(ble_das_t * p_das, const ble_das_init_t * p_das_init)
{
    ble_uuid_t ble_uuid;
    ble_uuid128_t base_uuid = {DAS_UUID_BASE};
    ble_uuid.uuid = DAS_UUID_SERVICE;
    p_das->conn_handle = BLE_CONN_HANDLE_INVALID;

    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &ble_uuid.type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_add_char_params_t add_char_params;

    // Initialize service structure
    p_das->write_evt_handler           = p_das_init->write_evt_handler;
    p_das->conn_handle                 = BLE_CONN_HANDLE_INVALID;
    p_das->max_len                     = MAX_DAS_LEN;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_das->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Cmd characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = CMD_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = MAX_DAS_LEN;
    add_char_params.init_len          = MAX_DAS_LEN;
    add_char_params.char_props.write  = 1;
//    add_char_params.char_props.read   = 1;
//    add_char_params.char_props.notify = 1;
    add_char_params.write_access      = SEC_OPEN;
//    add_char_params.read_access       = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;

    err_code = characteristic_add(p_das->service_handle, &add_char_params, &(p_das->cmd_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add response characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.is_var_len        = true;
    add_char_params.uuid              = RSP_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = MAX_DAS_LEN;
    add_char_params.init_len          = MAX_DAS_LEN;
//    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.char_props.notify = 1;
//    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;
    add_char_params.cccd_write_access = SEC_OPEN;

    err_code = characteristic_add(p_das->service_handle, &add_char_params, &(p_das->rsp_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}

uint32_t ble_das_rsp_notify(ble_das_t *p_das, uint8_t *data, uint16_t len) {
    uint32_t err_code;

    // Send value if connected and notifying
    if (p_das->conn_handle != BLE_CONN_HANDLE_INVALID && p_das->is_notification_enabled) {
        ble_gatts_hvx_params_t hvx_params;
        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_das->rsp_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &len;
        hvx_params.p_data = data;

        err_code = sd_ble_gatts_hvx(p_das->conn_handle, &hvx_params);
//        if (err_code == NRF_SUCCESS) {
//            NRF_LOG_HEXDUMP_DEBUG(data, len);
//            NRF_LOG_FLUSH();
//        }
    }
    else {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    if (err_code != NRF_SUCCESS && err_code != NRF_ERROR_RESOURCES) {
//        NRF_LOG_DEBUG("rsp notify error: %d", err_code);
    }

    return err_code;
}

uint32_t ble_das_rsp_write(ble_das_t *p_das, uint8_t *data, uint16_t len) {
    ret_code_t err_code;

    ble_gatts_value_t  gatts_value;
    memset(&gatts_value, 0, sizeof(gatts_value));

    gatts_value.p_value = data;
    gatts_value.len= len;

    err_code = sd_ble_gatts_value_set(p_das->conn_handle, p_das->rsp_handles.value_handle, &gatts_value);
    if (err_code != NRF_SUCCESS) {
        NRF_LOG_DEBUG("rsp write error: %d", err_code);
    }

    return err_code;
}

//uint32_t ble_das_txt_send(ble_das_t * p_das, char *str)
//{
//    uint32_t err_code;
//
//    // Send value if connected and notifying
//    if (p_das->conn_handle != BLE_CONN_HANDLE_INVALID && p_das->is_notification_enabled)
//    {                
//        uint16_t hvx_len = strlen(str);
//        ble_gatts_hvx_params_t hvx_params;
//
//        memset(&hvx_params, 0, sizeof(hvx_params));
//
//        hvx_params.handle = p_das->txt_handles.value_handle;
//        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
//        hvx_params.offset = 0;
//        hvx_params.p_len  = &hvx_len;
//        hvx_params.p_data = (uint8_t *)str;
//
//        err_code = sd_ble_gatts_hvx(p_das->conn_handle, &hvx_params);
//        if ((err_code != NRF_SUCCESS))
//        {
//            //err_code = NRF_ERROR_DATA_SIZE;
//            NRF_LOG_DEBUG("hvx error: %d", err_code);
//        }
//    }
//    else
//    {
//        err_code = NRF_ERROR_INVALID_STATE;
//    }
//
//    return err_code;
//}
