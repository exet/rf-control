#ifndef _MAIN_H_
#define _MAIN_H_

#include "limits.h"
#include "arm_math.h"

#define IIR_ARITHMETIC_FLOAT32      1
#define IIR_ARITHMETIC_Q15          2
#define IIR_ARITHMETIC              IIR_ARITHMETIC_FLOAT32

#if IIR_ARITHMETIC == IIR_ARITHMETIC_FLOAT32
typedef float32_t sense_data_t;
#elif IIR_ARITHMETIC == IIR_ARITHMETIC_Q15
typedef q15_t sense_data_t;
#endif


int16_t saturate_f_to_i16(float val);


#endif // _MAIN_H_