#include "gpio.h"

uint8_t LEDS_NUM[] = {LED_VD1, LED_VD2, LED_VD3, LED_R_VD4, LED_G_VD4, LED_B_VD4};
const uint8_t COUNT_LEDS_NUM = sizeof(LEDS_NUM)/sizeof(LEDS_NUM[0]);

uint8_t BUTS_NUM[] = {BUT_1, BUT_2, BUT_3, BUT_4, BUT_5};
const uint8_t COUNT_BUTS_NUM = sizeof(BUTS_NUM)/sizeof(BUTS_NUM[0]);

ret_code_t my_gpio_init(void) {
    
    for(int i = 0; i < COUNT_LEDS_NUM; i ++) {
        nrf_gpio_cfg_output(LEDS_NUM[i]);
        nrf_gpio_pin_write(LEDS_NUM[i], LED_OFF);
    }

    nrf_gpio_cfg_output(SPI_DPY_DC_PIN);
    nrf_gpio_cfg_output(SPI_DPY_RS_PIN);

    for(int i = 0; i < COUNT_BUTS_NUM; i ++) {
        nrf_gpio_cfg_input(BUTS_NUM[i], NRF_GPIO_PIN_NOPULL);
    }

}


void all_leds_off(void) {

    for(int i = 0; i < COUNT_LEDS_NUM; i ++) {
        nrf_gpio_pin_write(LEDS_NUM[i], LED_OFF);
    }

}